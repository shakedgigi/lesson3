/*********************************
* Class: MAGSHIMIM C++2		   *
* Week 3           	     	   *
* shaked gigi                  *
**********************************/

#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include "sqlite3.h"
#include <unordered_map>
#include <vector>

void print_table();

using namespace std;


unordered_map<string, vector<string>> results;


int main()
{
	int Askiname;
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;           

	rc = sqlite3_open("FirstPart.db", &db);     // connection to the database
	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}
   
	system("CLS");

	rc = sqlite3_exec(db, "create table people(int id primary key not null, string name);", NULL, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		cout << "progress made " << endl;
	}

	rc = sqlite3_exec(db, "insert into people(name) values('shaked');", NULL, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		cout << "progress made " << endl;
	}

	rc = sqlite3_exec(db, "insert into people(name) values('poop');", NULL, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		cout << "progress made " << endl;
	}

	rc = sqlite3_exec(db, "insert into people(name) values('heya');", NULL, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		cout << "progress made " << endl;
	}

	system("PAUSE");
	return (0);
}

void print_table()
{
	auto iter = results.end();
	iter--;
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				printf("|%*s|", 13, it->first.c_str());
			}
			if (i == -1)
			{
				cout << "_______________";
			}
			else
			{
				printf("|%*s|", 13, it->second.at(i).c_str());
			}
		}
		cout << endl;
	}

}